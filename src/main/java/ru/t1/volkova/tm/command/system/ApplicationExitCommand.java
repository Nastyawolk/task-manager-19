package ru.t1.volkova.tm.command.system;

import ru.t1.volkova.tm.api.service.ILoggerService;

public final class ApplicationExitCommand extends AbstractSystemCommand {

    private static final String ARGUMENT = null;

    private static final String DESCRIPTION = "Close application.";

    private static final String NAME = "exit";

    @Override
    public void execute() {
        final ILoggerService loggerService = getServiceLocator().getLoggerService();
        loggerService.info("** EXIT TASK MANAGER **");
        System.exit(0);
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

}
