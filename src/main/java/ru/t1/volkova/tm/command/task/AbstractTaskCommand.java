package ru.t1.volkova.tm.command.task;

import ru.t1.volkova.tm.api.service.IProjectTaskService;
import ru.t1.volkova.tm.api.service.ITaskService;
import ru.t1.volkova.tm.command.AbstractCommand;
import ru.t1.volkova.tm.enumerated.Status;
import ru.t1.volkova.tm.exception.entity.TaskNotFoundException;
import ru.t1.volkova.tm.model.Task;

import java.util.List;

public abstract class AbstractTaskCommand extends AbstractCommand {

    protected IProjectTaskService projectTaskService() {
        return getServiceLocator().getProjectTaskService();
    }

    protected ITaskService taskService() {
        return getServiceLocator().getTaskService();
    }

    @Override
    public String getArgument() {
        return null;
    }

    protected void renderTasks(final List<Task> tasks) {
        int index = 1;
        for (final Task task : tasks) {
            if (task == null) continue;
            String print = index++ + ". " +
                    task.getName() + " | " +
                    task.getId() + " | " +
                    task.getStatus();
            System.out.println(print);
        }
    }

    protected void showTask(final Task task) {
        if (task == null) throw new TaskNotFoundException();
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("STATUS: " + Status.toName(task.getStatus()));
    }

}
