package ru.t1.volkova.tm.api.service;

import ru.t1.volkova.tm.enumerated.Sort;
import ru.t1.volkova.tm.enumerated.Status;
import ru.t1.volkova.tm.model.Project;

import java.util.List;

public interface IProjectService extends IService<Project> {

    Project updateById(String id, String name, String description);

    Project updateByIndex(Integer index, String name, String description);

    Project changeProjectStatusById(String id, Status status);

    Project changeProjectStatusByIndex(Integer index, Status status);

    List<Project> findAll(Sort sort);

}
