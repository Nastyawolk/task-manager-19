package ru.t1.volkova.tm.api.repository;

import ru.t1.volkova.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IRepository<Task> {

    List<Task> findAllByProjectId(String projectId);

}
